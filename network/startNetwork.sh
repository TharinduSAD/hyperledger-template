# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

#removing all the containers
echo ""
echo "##### Removing all the containers #####"
echo ""
echo ""
docker rm -f $(docker ps -aq)
echo ""
echo ""

#prune the volumes
echo "#### Prune the network #####"
echo ""
echo ""
docker volume prune
echo ""
echo ""

#down the previously build networks
echo "##### Removing networks #####" 
echo ""
echo ""
docker network prune
echo ""
echo ""

#down the previously build networks
echo "##### Clearing the System #####" 
echo ""
echo ""
docker system prune
echo ""
echo ""

#Up the network
echo "##### Network is under the build #####"
echo ""
echo ""
docker-compose -f docker-compose-cli.yaml -f docker-compose-couch.yaml up -d
echo ""
echo ""

# echo "##### Network is under the build #####"
# echo ""
# echo ""
# docker-compose -f docker-compose-cli.yaml up -d
# echo ""
# echo ""

#starting the cli
echo ""
echo ""
echo "##### Starting the cli incase if it is sleep #####"
docker start cli
echo ""
echo ""


#creating the channel
echo ""
echo ""
echo "##### Creating the channel #####"
echo ""
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org1.example.com:7051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli peer channel create -o orderer.example.com:7050 -c myc -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
if [ "$?" -ne 0 ]; then
  echo "Channel Creation Failed...."
  exit 1
fi
echo ""
echo ""




# #joining the peers to the channel
# echo ""
# echo ""
# echo ""
# echo "##### Joining Org1 peeer0 to  the channel #####"
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org1.example.com:7051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli peer channel join -b myc.block
# if [ "$?" -ne 0 ]; then
#   echo "Joining channel failed for Org1 Peer0...."
#   exit 1
# fi
# echo ""
# echo ""

# echo ""
# echo ""
# echo "##### Joining Org1 peeer1 to  the channel #####"
# echo ""
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer1.org1.example.com:8051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli peer channel join -b myc.block
# if [ "$?" -ne 0 ]; then
#   echo "Joining channel failed for Org1 Peer1...."
#   exit 1
# fi
# echo ""
# echo ""


# echo ""
# echo ""
# echo ""
# echo "##### Joining Org2 peeer0 to  the channel #####"
# docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org2.example.com:9051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" cli peer channel join -b myc.block
# if [ "$?" -ne 0 ]; then
#   echo "Joining channel failed for Org2 Peer0...."
#   exit 1
# fi
# echo ""
# echo ""

# echo ""
# echo ""
# echo "##### Joining Org2 peeer1 to  the channel #####"
# echo ""
# docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp" -e "CORE_PEER_ADDRESS=peer1.org2.example.com:10051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" cli peer channel join -b myc.block
# if [ "$?" -ne 0 ]; then
#   echo "Joining channel failed for Org2 Peer1...."
#   exit 1
# fi
# echo ""
# echo ""




# #Updating the anchorpeers 
# echo ""
# echo ""
# echo "##### Updating anchor peer of Org1 #####"
# echo ""
# docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org1.example.com:7051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli peer channel update -o orderer.example.com:7050 -c myc -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
# if [ "$?" -ne 0 ]; then
#   echo "Failed anchor peer update for Org1...."
#   exit 1
# fi
# echo ""
# echo ""


# echo ""
# echo ""
# echo "##### Updating anchor peer of Org2 #####"
# echo ""
# docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org2.example.com:9051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" cli peer channel update -o orderer.example.com:7050 -c myc -f ./channel-artifacts/Org2MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
# if [ "$?" -ne 0 ]; then
#   echo "Failed anchor peer update for Org2...."
#   exit 1
# fi
# echo ""
# echo ""

