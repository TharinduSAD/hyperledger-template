export MSYS_NO_PATHCONV=1

echo ""
echo ""
echo ""
echo "##### Instantiating Chaincode #####"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org1.example.com:7051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli 	peer chaincode instantiate -o orderer.example.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C myc -n fabcar -l node -v 1.0 -c '{"Args":["init"]}'  -P "AND ('Org1MSP.peer','Org2MSP.peer')"
if [ "$?" -ne 0 ]; then
  echo "Instantiating Chaincode failed"
  exit 1
fi
echo ""
echo ""