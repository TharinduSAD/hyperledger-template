export MSYS_NO_PATHCONV=1

echo ""
echo ""
echo ""
echo "##### Installing chaincode on Org1 peeer0 #####"
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org1.example.com:7051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt" cli 	peer chaincode install -n fabcar -v 1.0 -l node -p /opt/gopath/src/github.com/chaincode/fabcar/javascript-low-level/
if [ "$?" -ne 0 ]; then
  echo "Installing chaincode on Org1 peeer0 failed"
  exit 1
fi
echo ""
echo ""

echo ""
echo ""
echo ""
echo "##### Installing chaincode on Org2 peer0 #####"
docker exec -e "CORE_PEER_LOCALMSPID=Org2MSP" -e "CORE_PEER_MSPCONFIGPATH=//opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp" -e "CORE_PEER_ADDRESS=peer0.org2.example.com:9051" -e "CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt" cli  peer chaincode install -n fabcar -v 1.0 -l node -p /opt/gopath/src/github.com/chaincode/fabcar/javascript-low-level/
if [ "$?" -ne 0 ]; then
  echo "Installing chaincode on Org2 peer0 Failed...."
  exit 1
fi
echo ""
echo ""